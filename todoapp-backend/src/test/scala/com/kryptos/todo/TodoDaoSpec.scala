package com.kryptos.todo

import doobie.imports._
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import com.kryptos.todo.dao.Database
import scalaz.concurrent.Task
import scalaz.stream.Process

/**
 * @author kryptos
 */
class TodoDaoSpec extends FlatSpec with Matchers {

  val tx = Database.tx
  val todoDao = Database.TodoDao
  
  "insert todo" should "insert a todo record" in {
    val result = todoDao.insert(Option("test"), Option("test")).transact(tx).run

    result should be > 0
  }
  
  "find todo" should "find by title test" in {
    val task: Process[Task, Database.Todo] = todoDao.findByTitle("test").transact(tx)
    task should not be null
    
    val todoObj : Database.Todo = task.runLog.run.head
    todoObj.title.get.trim() should be ("test")
    todoObj.description.get.trim() should be ("test")
  }

   "update todo" should "update todo description with testing" in {
    val todo = todoDao.findByTitle("test").transact(tx).runLog.run.head
    todoDao.update(todo.id, todo.title, Option("testing"), todo.status).transact(tx).run
    val todoUpdated = todoDao.findByTitle("test").transact(tx).runLog.run.head
    
    todoUpdated.description.get.trim() should be ("testing")
  }

  "delete todo" should "delete the test todo" in {
    val todo = todoDao.findByTitle("test").transact(tx).runLog.run.head
    val deleteResult = todoDao.delete(todo.id).transact(tx).run
    
    deleteResult should be > 0
    
    a [Exception] should be thrownBy {
     todoDao.findByTitle("test").transact(tx).runLog.run.head
    }
  }

  "all todos" should "at least get some todos" in {
    todoDao.insert(Option("test1"), Option("test")).transact(tx).run
    todoDao.insert(Option("test2"), Option("test")).transact(tx).run
    todoDao.insert(Option("test3"), Option("test")).transact(tx).run
    
    todoDao.all.list.transact(tx).run.length should be > 1
  }

}
