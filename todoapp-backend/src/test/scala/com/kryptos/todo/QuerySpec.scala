package com.kryptos.todo

import doobie.imports._
import doobie.contrib.specs2.analysisspec.AnalysisSpec
import org.specs2.mutable.Specification
import com.kryptos.todo.dao.Database
import scalaz.concurrent.Task

/**
 * @author kryptos
 */
object QuerySpec extends Specification with AnalysisSpec {
  
  val transactor = DriverManagerTransactor[Task](
    "org.postgresql.Driver", "jdbc:postgresql:tododb", "todo", "todo"
  )
  
  val todoQueries =  Database.TodoQueries
  
  check(todoQueries.all)
  check(todoQueries.findOne(0))
  check(todoQueries.findByTitle(""))
  check(todoQueries.insert(Option(""), Option("")))
  check(todoQueries.update(0, Option(""), Option(""), Option("")))
  check(todoQueries.delete(0))
  
}