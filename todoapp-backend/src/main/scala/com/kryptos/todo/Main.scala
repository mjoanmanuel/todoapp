package com.kryptos.todo

import org.http4s.server.blaze.BlazeBuilder
import com.kryptos.todo.endpoint.TodoEndpoint
import org.http4s.server.middleware.CORS
import org.http4s.server.middleware.CORSConfig

/**
 * @author kryptos
 */
object Main extends App {
  println("rocking with scala.")
  
  val corsConfig = CORSConfig(true, false, 10000, true, Some(Set("*")))
  val cors = CORS.apply(TodoEndpoint.service, corsConfig);
  
  BlazeBuilder
    .bindLocal(8080)
    .mountService(cors, "/todoapp")
    .run
    .awaitShutdown();
}