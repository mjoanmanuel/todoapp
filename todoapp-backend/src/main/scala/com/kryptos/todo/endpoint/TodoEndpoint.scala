package com.kryptos.todo.endpoint

import org.http4s.Request
import org.http4s.UrlForm
import org.http4s.dsl._
import org.http4s.server._
import org.json4s._
import org.json4s.native.Serialization
import org.json4s.native.Serialization.write
import org.json4s.NoTypeHints
import scalaz.concurrent.Task
import scalaz.stream._

import doobie.imports._
import com.kryptos.todo.dao.Database

/**
 * @author kryptos
 */
object TodoEndpoint {

  implicit val formats = Serialization.formats(NoTypeHints)
  
  val todoDao = Database.TodoDao
  val tx = Database.tx

  def stream(): Process[Task, String] = {
    val todos = todoDao.all
      .map(write(_))
      .intersperse(",")
      .transact(tx)

    Process.emit("[") ++ todos ++ Process.emit("]")
  }

  val service = HttpService {

    case GET -> Root / "all" =>
      Ok(stream())

    case req @ DELETE -> Root / "delete" =>
      req.decode[UrlForm] { data =>
        val id = data.values("id").head.toLong
        todoDao.delete(id).transact(tx).run
        Ok("delete TODO.")
      }

    case req @ POST -> Root / "create" =>
      req.decode[UrlForm] { data =>
        val title = data.values("title").head.trim()
        val desc = data.values("description").head.trim()
        val status = data.values("status").head.trim()

        if (data.values.contains("id")) {
          val id = data.values("id").head.toLong
          todoDao
            .update(id, Option(title), Option(desc), Option(status))
            .transact(tx)
            .run
        } else {
          todoDao
            .insert(Option(title), Option(desc))
            .transact(tx)
            .run
        }

        Ok("Todo added")
      }
  }

}