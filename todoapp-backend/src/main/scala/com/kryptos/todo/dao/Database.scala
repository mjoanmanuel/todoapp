package com.kryptos.todo.dao

import doobie.imports._
import scalaz._
import Scalaz._
import scalaz.concurrent.Task
import scalaz.stream.Process

/**
 * @author kryptos
 */
object Database {

  val tx =
    DriverManagerTransactor[Task](
      "org.postgresql.Driver", "jdbc:postgresql:tododb", "todo", "todo")

      
  // model
  case class Todo (
      id: Long,
      title: Option[String], 
      description: Option[String], 
      status: Option[String] = Option("PENDING")
  )

  object TodoQueries {
    
    def all : Query0[Todo] =
      sql"select id, title, description, status from todos order by id"
      .query[Todo]

   def findOne(id: Long) : Query0[Todo] =
     sql"select id, title, description, status from todos where id = $id"
       .query[Todo]


   def findByTitle(title: String) : Query0[Todo] = 
     sql"select id, title, description, status from todos where title = $title"
       .query[Todo]
    
   def insert(title: Option[String], description: Option[String]) : Update0 = 
     sql"insert into todos (title, description, status) values ($title, $description, 'PENDING')".update

   def update(id: Long, title: Option[String], description: Option[String], status: Option[String]) : Update0 = 
     sql"update todos set title = $title, description = $description, status = $status where id = $id".update

   def delete(id: Long) : Update0 = 
     sql"delete from todos where id = $id".update
  }
  
  object TodoDao {

    def all: Process[ConnectionIO, Todo] = TodoQueries.all.process

    def findOne(id: Long): Process[ConnectionIO, Todo] = TodoQueries.findOne(id).process

    def findByTitle(title: String): Process[ConnectionIO, Todo] = TodoQueries.findByTitle(title).process

    def insert(title: Option[String], description: Option[String]): ConnectionIO[Int] =
      TodoQueries.insert(title, description).run

    def update(id: Long, title: Option[String], description: Option[String], status: Option[String]): ConnectionIO[Int] =
      TodoQueries.update(id, title, description, status).run

    def delete(id: Long): ConnectionIO[Int] =
      TodoQueries.delete(id).run
  }

}