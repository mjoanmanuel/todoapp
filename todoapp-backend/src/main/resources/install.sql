CREATE SEQUENCE todos_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE todos_seq
  OWNER TO todo;

CREATE TABLE todos
(
  id bigint NOT NULL DEFAULT nextval('todos_seq'::regclass),
  title character(50),
  description character(500),
  status character(10),
  CONSTRAINT todo_primary_key PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE todos
  OWNER TO postgres;
GRANT ALL ON TABLE todos TO postgres;
GRANT ALL ON TABLE todos TO todo;

