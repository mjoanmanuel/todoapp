name := "todoapp"
version := "1.0"
scalaVersion := "2.11.7"

resolvers ++= Seq(
  "tpolecat" at "http://dl.bintray.com/tpolecat/maven",
  "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"
)

def http4sVersion = "0.10.0"
def doobieVersion = "0.2.2"

libraryDependencies ++= Seq( 
	"org.json4s" %% "json4s-native" % "3.2.11",
	
	"org.http4s" % "http4s-core_2.11" % http4sVersion,
	"org.http4s" % "http4s-dsl_2.11" % http4sVersion,
	"org.http4s" % "http4s-blaze-server_2.11" % http4sVersion,
	
	"org.tpolecat" %% "doobie-core" % doobieVersion,
	"org.tpolecat" %% "doobie-contrib-postgresql" % doobieVersion,
	"org.tpolecat" %% "doobie-contrib-specs2"   % doobieVersion,
	
	"org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"
)
