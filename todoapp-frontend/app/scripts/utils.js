angular.module('todoApp')
.service('Utils', ['$http','$q', 'ENDPOINT_URI',
    function ($http, $q, ENDPOINT_URI) {
	
	service = this;

	service.get = function(service){
        var d = $q.defer();

        $http.get(ENDPOINT_URI + service)
            .success(function(data) {
                d.resolve(data);
            })
            .error(function(data, status){
                var reason = {
                    responseData: data,
                    responseStatus: status
                };
                d.reject(reason);
            });

        return d.promise;
	};

	service.post = function(service, object) {
        return request(service, object, 'POST');
	};

    service.delete = function(service, object) {
        return request(service, object, 'DELETE');
    };

    function request(service, object, method) {
        var d = $q.defer();

        $http({
            method: method,
            url: ENDPOINT_URI + service,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: object
        }).success(function (data) {
            d.resolve(data);
        }).error(function(data, status){
            var reason = {
              responseData: data,
              responseStatus: status
            };
            d.reject(reason);
        });

        return d.promise;
    }

	
}]);
