'use strict';

/**
 * @ngdoc function
 * @name kindleitApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the kindleitApp
 */
var module = angular.module('todoApp')

module.controller('TodoCtrl', 
  	['$scope','$modal', 'TodoService', function ($scope, $modal, TodoService) {

  	$scope.todos = [];

  	function allTodos(){
  		$scope.todos = [];
	  	TodoService.allTodos().then(function(data){
	  		angular.forEach(data, function(todo){
	  			var obj = {
	  				id : todo.id,
	  				title : todo.title.trim(),
	  				description : todo.description.trim(),
	  				status : todo.status.trim()
	  			}

	  			$scope.todos.push(obj);
	  		});

	  	}).catch(function(reason){
	  		console.log(reason);
	  	});
  	}

  	$scope.addTodo = function() {
  		$scope.editTodo(null)
  	}

  	$scope.editTodo = function(todo) {
  		var modalInstance = $modal.open({
            templateUrl : 'views/todo-maintenance.html',
            controller : 'TodoMaintenanceCtrl',
            size : 'lg',
            resolve : {
                'todo' : function () {
                    return todo; 
                }
            }
  		});

  		modalInstance.result.then(function(){
  			allTodos();
  		});
  	}

  	$scope.deleteTodo = function(todo) {
  		TodoService.delete(todo).then(function(){
  			alert(todo.id + ' was deleted.');
  			allTodos();
  		}).catch(function(reason){
  			console.log(reason);
  		});
  	}

  	allTodos();

  }]);

module
	.controller('TodoMaintenanceCtrl', function($scope, $modalInstance, TodoService, todo) {
		$scope.todo = null;

		if(todo != null) {
			$scope.todo = todo;
		} else {
			$scope.todo = {
				title : null,
				description : null,
				status : null
			}
		}

		$scope.createOrUpdate = function() {
			TodoService.createOrUpdate($scope.todo).then(function(data){
				$modalInstance.close('Ok');
			}).catch(function(reason){
				console.log(reason);
			});
		}

		$scope.cancel = function() {
			$modalInstance.dismiss();
		}
});