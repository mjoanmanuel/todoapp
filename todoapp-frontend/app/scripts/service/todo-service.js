'use strict';

angular.module('todoApp')
  .service('TodoService',
     ['Utils',
    function(Utils){
      var service = this;

      service.allTodos = function() {
        return Utils.get('all');
      };

      service.delete = function(obj) {
        return Utils.delete('delete', obj);
      };

      service.createOrUpdate = function(obj) {
        return Utils.post('create', obj);
      };

      
}]);