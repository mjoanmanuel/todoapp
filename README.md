# todoapp

A simple todo application using the following stack
*frontend
- angularjs
- bootstrap

*backend
-scala
-http4s
-doobie
-postgres

# installation
*frontend
I use yeoman as a frontend scaffolding tool.

*assuming you have installed
-nodejs
-grunt
-bower

1- > npm install
2- > bower install
3- > grunt serve (will popup a browser with the following URL http://localhost:9000/#/)

*backend
I used SBT to build the project. 

*assuming you have installed
- sbt

1-> sbt run (run on http://localhost:8080)

